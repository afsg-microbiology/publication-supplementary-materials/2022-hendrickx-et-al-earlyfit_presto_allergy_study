***R code for Earlyfit PRESTO allergy study***

***Title: ‘Assessment of infant outgrowth of cow’s milk allergy in relation to the faecal microbiome and metaproteome’***

***Creators***
Diana M. Hendrickx 1,*, Ran An 1,+, Sjef Boeren 2, Sumanth K Mutte 2, PRESTO study team #, Jolanda M Lambert 3, and 
Clara Belzer 1
1	Laboratory of Microbiology, Wageningen University, Wageningen, The Netherlands
2	Laboratory of Biochemistry, Wageningen University, Wageningen, The Netherlands
3	Danone Nutricia Research, Utrecht, The Netherlands

+ current address: Department of Food Science and Technology, School of Agriculture and Biology, Shanghai Jiao Tong University, 
Shanghai 200240, China.

* Correspondence: diana.hendrickx@wur.nl; clara.belzer@wur.nl 

# PRESTO study team: A list of authors and their affiliations is listed in the manuscript.

***Related publication***
Hendrickx, D.M., An, R., Boeren, S., Mutte S.K.,  PRESTO study team, Lambert J.M. & Belzer C. Assessment of infant outgrowth of cow’s milk allergy in relation to the faecal microbiome and metaproteome. Sci Rep 13, 12029 (2023).
DOI: 10.1038/s41598-023-39260-w
Received: 17 October 2022; Accepted: 22 July 2023; Published: 25 July 2023


***General Introduction***
This folder contains R scripts for the statistical analysis within the Earlyfit PRESTO allergy study.
This study was part of the EARLYFIT project (Partnership programme NWO Domain AES-Danone Nutricia Research), funded by the Dutch Research Council (NWO) and Danone Nutricia Research (project number: 16490).

***Data availability***
Clinical data are available from Danone Nutricia research upon reasonable request (contact: Harm Wopereis, Danone Nutricia Research, Utrecht, The Netherlands, Harm.Wopereis@danone.com).
16S-rRNA gene sequencing data are available in the European Nucleotide Archive (http://www.ebi.ac.uk/ena) under study accession PRJEB56782.
The mass spectrometry proteomics data have been deposited to the ProteomeXchange Consortium via the PRIDE partner repository with the dataset identifier PXD037190.




