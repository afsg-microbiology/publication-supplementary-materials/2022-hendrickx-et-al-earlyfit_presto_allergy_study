#########################################################################
#Linear mixed model (LMM) analysis on core taxa, with outgrowth of CMA, #
#visit and outgrowth of CMA x visit as fixed effects and subject as     #
#random effect.                                                         # 
#########################################################################
#Author: Diana Hendrickx#
#########################
#input:
#- data_IBAQ_families_rel.RData - R workspace (.RData file) with relative 
#  abundances at the family level. Rows are taxa, columns are samples.
#- metadata.txt: metadata which contains the following columns:
#  "DAS_number" (sample ID),"Subject number","Visit","TOLCMVIS 12M"
#  (tolerance to cow's milk Yes/No). Rows are samples.

#import the metadata.txt file into R
#load data_IBAQ_families_rel.RData

#set work directory to directory with input .biom and .csv files
setwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")

#load R packages
library(readxl)
library(limma)
library(stringi)
library(dplyr)
library(compositions)
library(lme4)
library(lmerTest)
library(emmeans)

##As we will apply t-tests to compare visits, we have to normalize
#the data with CLR transform.
#clr transform #lowest rel abundance order 10^(-5)
clr_IBAQ_families_rel<-matrix(clr(t(data_IBAQ_families_rel+10^(-6))),
                              nrow=117,ncol=19)

rownames(clr_IBAQ_families_rel)<-colnames(data_IBAQ_families_rel)
colnames(clr_IBAQ_families_rel)<-rownames(data_IBAQ_families_rel)

meta<-data.frame()
for (i in 1:117){
  df<-metadata[which(metadata$DAS_number==rownames(clr_IBAQ_families_rel)[i]),3:6]
  meta<-rbind(meta,df)
}
rownames(meta)<-rownames(clr_IBAQ_families_rel)
data_LMM<-data.frame(clr_IBAQ_families_rel,meta)

#Run LMM per family
pvals_compare_visits<-data.frame(family=character(),
                                 contrast=character(),
                                 tolerance_CM=character(),
                                 pval=numeric())
pvals_compare_intervention_groups<-data.frame(family=character(),
                                              contrast=character(),
                                              visit=character(),
                                              pval=numeric())

for (i in 1:19){
  data_LMER<-data.frame(family=data_LMM[,i],
                        data_LMM[,20:21],
                        TOLCMVIS.12M=data_LMM[,23])
  mixed_lmer<-lmer(family ~ TOLCMVIS.12M +
                     Visit + TOLCMVIS.12M*Visit + (1|Subject.number), 
                   data = data_LMER)
  test1<-pairs(emmeans(mixed_lmer,
                       "Visit", by = "TOLCMVIS.12M"))
  test2<-pairs(emmeans(mixed_lmer,
                       "TOLCMVIS.12M", by = "Visit"))
  df1<-data.frame(family=colnames(data_LMM)[i],
                  contrast=summary(test1)$contrast,
                  tolerance_CM=summary(test1)$TOLCMVIS.12M,
                  pval=summary(test1)$p.value)
  df2<-data.frame(family=colnames(data_LMM)[i],
                  contrast=summary(test2)$contrast,
                  visit=summary(test2)$Visit,
                  pval=summary(test2)$p.value)
  pvals_compare_visits<-rbind(pvals_compare_visits,df1)
  pvals_compare_intervention_groups<-rbind(pvals_compare_intervention_groups,df2)
}

Y_N_T0M<-pvals_compare_intervention_groups[pvals_compare_intervention_groups$visit=="T0M",]
Y_N_T0M$adjpval<-p.adjust(Y_N_T0M$pval,method = "BH")
which(Y_N_T0M$adjpval<=0.05)
#=> nothing significant

Y_N_T6M<-pvals_compare_intervention_groups[pvals_compare_intervention_groups$visit=="T6M",]
Y_N_T6M$adjpval<-p.adjust(Y_N_T6M$pval,method = "BH")
which(Y_N_T6M$adjpval<=0.05)
#=> nothing significant

Y_N_T12M<-pvals_compare_intervention_groups[pvals_compare_intervention_groups$visit=="T12M",]
Y_N_T12M$adjpval<-p.adjust(Y_N_T12M$pval,method = "BH")
which(Y_N_T12M$adjpval<=0.05)
#=> nothing significant

T0M_T6M_Y<-pvals_compare_visits[which(pvals_compare_visits$contrast=="T0M - T6M"
                                      & pvals_compare_visits$tolerance_CM=="Yes"),]
T0M_T6M_Y$adjpval<-p.adjust(T0M_T6M_Y$pval,method = "BH")
ind_sign_T0M_T6M_Y<-which(T0M_T6M_Y$adjpval<=0.05)
#=> nothing significant

T0M_T12M_Y<-pvals_compare_visits[which(pvals_compare_visits$contrast=="T0M - T12M"
                                       & pvals_compare_visits$tolerance_CM=="Yes"),]
T0M_T12M_Y$adjpval<-p.adjust(T0M_T12M_Y$pval,method = "BH")
ind_sign_T0M_T12M_Y<-which(T0M_T12M_Y$adjpval<=0.05)
#calculate quantiles for relative abundances
ind_Y_0M<-which(meta$Visit=="T0M" & meta$TOLCMVIS.12M=="Yes")
tolY_0M<-data_IBAQ_families_rel[,ind_Y_0M]
ind_Y_12M<-which(meta$Visit=="T12M" & meta$TOLCMVIS.12M=="Yes")
tolY_12M<-data_IBAQ_families_rel[,ind_Y_12M]
quantiles_tolY_0M<-apply(tolY_0M[ind_sign_T0M_T12M_Y,],1,quantile)
quantiles_tolY_12M<-apply(tolY_12M[ind_sign_T0M_T12M_Y,],1,quantile)
table_sign_T0M_T12M_Y<-data.frame(family=colnames(quantiles_tolY_0M),
                                  Q1_0M=quantiles_tolY_0M[2,],
                                  med_0M=quantiles_tolY_0M[3,],
                                  Q3_0M=quantiles_tolY_0M[4,],
                                  Q1_12M=quantiles_tolY_12M[2,],
                                  med_12M=quantiles_tolY_12M[3,],
                                  Q3_12M=quantiles_tolY_12M[4,],
                                  pval=T0M_T12M_Y$pval[ind_sign_T0M_T12M_Y],
                                  adjpval=T0M_T12M_Y$adjpval[ind_sign_T0M_T12M_Y])
write.csv(table_sign_T0M_T12M_Y,"table_sign_T0M_T12M_Y.csv",row.names = FALSE)

T6M_T12M_Y<-pvals_compare_visits[which(pvals_compare_visits$contrast=="T12M - T6M"
                                       & pvals_compare_visits$tolerance_CM=="Yes"),]
T6M_T12M_Y$adjpval<-p.adjust(T6M_T12M_Y$pval,method = "BH")
ind_sign_T6M_T12M_Y<-which(T6M_T12M_Y$adjpval<=0.05)
#=> nothing significant

T0M_T6M_N<-pvals_compare_visits[which(pvals_compare_visits$contrast=="T0M - T6M"
                                      & pvals_compare_visits$tolerance_CM=="No"),]
T0M_T6M_N$adjpval<-p.adjust(T0M_T6M_N$pval,method = "BH")
ind_sign_T0M_T6M_N<-which(T0M_T6M_N$adjpval<=0.05)
#=> nothing significant

T0M_T12M_N<-pvals_compare_visits[which(pvals_compare_visits$contrast=="T0M - T12M"
                                       & pvals_compare_visits$tolerance_CM=="No"),]
T0M_T12M_N$adjpval<-p.adjust(T0M_T12M_N$pval,method = "BH")
ind_sign_T0M_T12M_N<-which(T0M_T12M_N$adjpval<=0.05)
#=> nothing significant

T6M_T12M_N<-pvals_compare_visits[which(pvals_compare_visits$contrast=="T12M - T6M"
                                       & pvals_compare_visits$tolerance_CM=="No"),]
T6M_T12M_N$adjpval<-p.adjust(T6M_T12M_N$pval,method = "BH")
ind_sign_T6M_T12M_N<-which(T6M_T12M_N$adjpval<=0.05)
#=> nothing significant

#write p-value tables
write.csv(Y_N_T0M,"Y_N_T0M.csv",row.names = FALSE)
write.csv(Y_N_T6M,"Y_N_T6M.csv",row.names = FALSE)
write.csv(Y_N_T12M,"Y_N_T12M.csv",row.names = FALSE)
write.csv(T0M_T6M_Y,"T0M_T6M_Y.csv",row.names = FALSE)
write.csv(T0M_T12M_Y,"T0M_T12M_Y.csv",row.names = FALSE)
write.csv(T6M_T12M_Y,"T6M_T12M_Y.csv",row.names = FALSE)
write.csv(T0M_T6M_N,"T0M_T6M_N.csv",row.names = FALSE)
write.csv(T0M_T12M_N,"T0M_T12M_N.csv",row.names = FALSE)
write.csv(T6M_T12M_N,"T6M_T12M_N.csv",row.names = FALSE)