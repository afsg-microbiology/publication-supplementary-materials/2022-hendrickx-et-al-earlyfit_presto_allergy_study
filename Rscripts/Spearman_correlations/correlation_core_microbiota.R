################################################################################
#Calculate Spearman correlations between 16S rRNA gene-based relative          #
#abundances of core taxa and metaproteomics-based relative abundances of       #
#core taxa. Calculate p-value based on permutation test.                       #
################################################################################
#Author: Diana Hendrickx#
#########################
#input:
#An .RData file including:
#- rel_abundance_core_16S: 16S rRNA gene-based relative abundances of core taxa.
#  Rows are taxa, columns are samples
#- rel_abundance_core_metaprot: metaproteomics-based relative abundances of core
#  taxa. Rows are taxa, columns are samples.
#Samples of rel_abundance_core_16S and rel_abundance_core_metaprot are in the
#same order.
#Taxa are in the order Bifidobacteriaceae, Coriobacteriaceae, Bacteroidaceae,
#Lachnospiraceae, Ruminococcaceae, Veillonellaceae, Enterobacteriaceae.

#set work directory to directory with input files
setwd("PATH_TO_WORK_DIRECTORY_WITH_INPUT")

library(coin)

#1 "Bifidobacteriaceae"
x1<-rel_abundance_core_16S[1,]
y1<-rel_abundance_core_metaprot[1,]
data1<-data.frame(x=x1,y=y1)
cor(x1,y1,method="spearman") 
spearman_test(x~y,data1,distribution=approximate(nresample = 10000))

#2 "Coriobacteriaceae"
x2<-rel_abundance_core_16S[2,]
y2<-rel_abundance_core_metaprot[2,]
data2<-data.frame(x=x2,y=y2)
cor(x2,y2,method="spearman")
spearman_test(x~y,data2,distribution=approximate(nresample = 10000))

#3 "Bacteroidaceae"
x3<-rel_abundance_core_16S[3,]
y3<-rel_abundance_core_metaprot[3,]
data3<-data.frame(x=x3,y=y3)
cor(x3,y3,method="spearman")
spearman_test(x~y,data3,distribution=approximate(nresample = 10000))

#4 "Lachnospiraceae"
x4<-rel_abundance_core_16S[4,]
y4<-rel_abundance_core_metaprot[4,]
data4<-data.frame(x=x4,y=y4)
cor(x4,y4,method="spearman")
spearman_test(x~y,data4,distribution=approximate(nresample = 10000))

#5 "Ruminococcaceae"
x5<-rel_abundance_core_16S[5,]
y5<-rel_abundance_core_metaprot[5,]
data5<-data.frame(x=x5,y=y5)
cor(x5,y5,method="spearman") 
spearman_test(x~y,data5,distribution=approximate(nresample = 10000))

#6 "Veillonellaceae"
x6<-rel_abundance_core_16S[6,]
y6<-rel_abundance_core_metaprot[6,]
data6<-data.frame(x=x6,y=y6)
cor(x6,y6,method="spearman")
spearman_test(x~y,data6,distribution=approximate(nresample = 10000))

#7 "Enterobacteriaceae"
x7<-rel_abundance_core_16S[7,]
y7<-rel_abundance_core_metaprot[7,]
data7<-data.frame(x=x7,y=y7)
cor(x7,y7,method="spearman")
spearman_test(x~y,data7,distribution=approximate(nresample = 10000))